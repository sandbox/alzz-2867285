<?php

/**
 * @file
 * Provides a stream wrapper and formatters appropriate for accessing and
 * displaying OlympicChannel videos.
 */

/**
 * Implements hook_media_internet_providers().
 */
function media_olympicchannel_media_internet_providers() {
  return array(
    'MediaInternetOlympicChannelHandler' => array(
      'title' => t('Olympic Channel'),
    ),
  );
}

/**
 * Implements hook_stream_wrappers().
 */
function media_olympicchannel_stream_wrappers() {
  return array(
    'olympicchannel' => array(
      'name' => t('Olympic channels videos'),
      'class' => 'MediaOlympicChannelStreamWrapper',
      'description' => t('Remote videos hosted on the Olympic Channel website.'),
      'type' => STREAM_WRAPPERS_READ_VISIBLE,
    ),
  );
}

/**
 * Implements hook_theme().
 */
function media_olympicchannel_theme($existing, $type, $theme, $path) {
  return array(
    'media_olympicchannel_video' => array(
      'variables' => array('uri' => NULL, 'options' => array()),
      'file' => 'media_olympicchannel.theme.inc',
      'path' => $path . '/theme',
    ),
  );
}

/**
 * Implements hook_media_parse().
 *
 * @todo This hook should be deprecated. Refactor Media module to not call it
 *   any more, since media_internet should be able to automatically route to the
 *   appropriate handler.
 */
function media_olympicchannel_media_parse($embed_code) {
  $handler = new MediaInternetOlympicChannelHandler($embed_code);
  
  return $handler->parse($embed_code);
}

/**
 * Implements hook_file_mimetype_mapping_alter().
 */
function media_olympicchannel_file_mimetype_mapping_alter(&$mapping) {
  $mapping['mimetypes'][] = 'video/olympicchannel';
}

/**
 * Implements hook_ctools_plugin_api().
 */
function media_olympicchannel_ctools_plugin_api($module, $api) {
  if ($module == 'file_entity' && $api == 'file_default_displays') {
    return array('version' => 1);
  }
}

/**
 * Implements hook_file_formatter_info().
 */
function media_olympicchannel_file_formatter_info() {
  $formatters['media_olympicchannel_video'] = array(
    'label' => t('Olympic Channel Video'),
    'file types' => array('video'),
    'default settings' => array(),
    'view callback' => 'media_olympicchannel_file_formatter_video_view',
    'settings callback' => 'media_olympicchannel_file_formatter_video_settings',
    'mime types' => array('video/olympicchannel'),
  );

  return $formatters;
}

/**
 * Implements hook_file_formatter_FORMATTER_view().
 */
function media_olympicchannel_file_formatter_video_view($file, $display, $langcode) {
  $scheme = file_uri_scheme($file->uri);

  // WYSIWYG does not yet support video inside a running editor instance.
  if ($scheme == 'olympicchannel' && empty($file->override['wysiwyg'])) {
    // Add js libraries to embed de Olympic Channel iframe.
    // @TODO: Where is the optimal hook to load the js.
    $path = drupal_get_path('module', 'media_olympicchannel');
    drupal_add_js('https://www.olympicchannel.com/static/js/syndicated/v1/embed.js', 'external');
    drupal_add_js($path . '/js/media_olympicchannel_video.js', 'file');

    $element = array(
      '#theme' => 'media_olympicchannel_video',
      '#uri' => $file->uri,
      '#options' => array(),
    );

    // Fake a default for attributes so the ternary doesn't choke.
    $display['settings']['attributes'] = array();

    foreach (array('width', 'height') as $setting) {
      $element['#options'][$setting] = isset($file->override[$setting]) ? $file->override[$setting] : $display['settings'][$setting];
    }

    return $element;
  }
}

/**
 * Implements hook_file_formatter_FORMATTER_settings().
 */
function media_olympicchannel_file_formatter_video_settings($form, &$form_state, $settings) {
  $element = array();

  $element['width'] = array(
    '#title' => t('Width'),
    '#type' => 'textfield',
    '#default_value' => $settings['width'],
    '#field_suffix' => t('pixels'),
  );

  $element['height'] = array(
    '#title' => t('Height'),
    '#type' => 'textfield',
    '#default_value' => $settings['height'],
    '#field_suffix' => t('pixels'),
  );

  return $element;
}
