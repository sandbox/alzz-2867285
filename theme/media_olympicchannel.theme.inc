<?php

/**
 * @file
 * Theme and preprocess functions for Media: Olympic Channel.
 */

/**
 * Theme Olympic Channel video.
 */
function theme_media_olympicchannel_video($variables) {
  // Build the URI.
  $wrapper = file_stream_wrapper_get_instance_by_uri($variables['uri']);
  $video_id = $wrapper->get_parameters();

  // Olympic channel js needs a container with the video id.
  $output = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array('olympicchannel-video'),
      'id' => $video_id,
      'data-id' => $video_id,
      'data-width' => $variables['options']['width'],
      'data-height' => $variables['options']['height'],
    ),
  );

  return drupal_render($output);
}
