CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Usage
 

INTRODUCTION
------------

Adds Olympic Channel as a supported media provider.


REQUIREMENTS
------------

Media: Olympic Channel has one dependency.

Contributed modules
 * Media Internet - A submodule of the Media module.

 
INSTALLATION
------------

Media: Olympic Channel can be installed via the standard Drupal installation process
(http://drupal.org/node/895232).


USAGE
-----

Media: Olympic Channel integrates https://www.olympicchannel.com video sharing
service with the Media module to allow users to add and manage Olympic Channel
videos as they would any other piece of media.

Internet media can be added on the Web tab of the Add file page (file/add/web).
With Media: Olympic Channel enabled, users can add a Olympic Channel video 
by entering its video code.

At this moment for embedding a video from https://www.olympicchannel.com you 
need to be whitelisted, so you need permission from them.
For the moment you have to enter a video id instead the video url, its how
its api works now.
Example: epis_a4758732d04c4a24b027d18a427442f0
The video id has the structure of epis_ID


