/**
 * @file
 * Javascript media Olympic Channel video.
 */

(function ($) {
  'use strict';
  Drupal.behaviors.media_olympicchannel_video = {
    attach: function (context, settings) {
      $('.olympicchannel-video').once('media-olympicchannel-video-processed', function () {
        var video = $(this);
        var player = new OLYMPIC_CHANNEL.Syndicated(video.attr('data-id'), {
          dimensions: [video.attr('data-width'), video.attr('data-height')],
          id: video.attr('id')
        });
      });
    }
  };
})(jQuery);
