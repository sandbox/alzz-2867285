<?php

/**
 * @file
 * Extends the MediaInternetBaseHandler class to handle Olympic channel videos.
 */

/**
 * Implementation of MediaInternetBaseHandler.
 *
 * @see hook_media_internet_providers().
 */
class MediaInternetOlympicChannelHandler extends MediaInternetBaseHandler {

  /**
   * Parse de user submitted code to identify if is a olympic channel video.
   *
   * @param string $embedCode
   *   A string of user-submitted embed code.
   *
   * @return string
   *   The normalized URI.
   */
  public function parse($embedCode) {
    // Olympic channel videos url doesnt has the video id.
    // @NOTE: User insert the video url and scrap the video if from html?
    $patterns = array(
      '@epis_([^"\&\? ]+)@i',
    );

    foreach ($patterns as $pattern) {
      preg_match($pattern, $embedCode, $matches);
      if (isset($matches[0])) {
        return file_stream_wrapper_uri_normalize('olympicchannel://v/' . $matches[0]);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function claim($embedCode) {
    if ($this->parse($embedCode)) {
      return TRUE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFileObject() {
    $uri = $this->parse($this->embedCode);
    $file = file_uri_to_object($uri, TRUE);
    $file->filemime = 'video/olympicchannel';
    $file->type = 'video';

    return $file;
  }

}
