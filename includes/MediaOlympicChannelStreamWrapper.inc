<?php

/**
 * @file
 * Extends MediaReadOnlyStreamWrapper class to handle Olympic channel videos.
 */

/**
 * Class for Resource Stream Wrappers.
 *
 * Create an instance like this:
 * $oc = new MediaOlympicChannelStreamWrapper('olympicchannel://v/[video-id]');
 */
class MediaOlympicChannelStreamWrapper extends MediaReadOnlyStreamWrapper {

  /**
   * {@inheritdoc}
   */
  public static function getMimeType($uri, $mapping = NULL) {
    return 'video/olympicchannel';
  }

}
